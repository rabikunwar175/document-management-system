<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix'=>'home','middleware'=>['role:superadministrator']],function(){
   Route::get('/', 'HomeController@index')->name('home');
});

// Route::get('/redirect','auth\Logincontroller@redirect');

Route::get('/home1', 'documentController@create')->name('home1');

Route::get('/departments','DepartmentController@create');
Route::post('departments','DepartmentController@store');
Route::get('/deptlist','DepartmentController@index');
Route::get('/edit/deptlist/{id}','DepartmentController@edit');
Route::post('/edit/deptlist/{id}','DepartmentController@update');
Route::delete('delete/deptlist/{id}','DepartmentController@destroy');


Route::get('/user','userController@create');
Route::post('user','userController@store');
Route::get('/list','userController@index');
Route::get('/edit/list/{id}','userController@edit');
Route::post('/edit/list/{id}','userController@update');
Route::delete('delete/list/{id}','userController@destroy');

Route::get('/document','documentController@create');
Route::get('/category','categoryController@create');
Route::post('category', 'categoryController@store');
Route::get('/catlist','categoryController@index');
Route::get('/edit/catlist/{id}', 'categoryController@edit');
Route::post('/edit/catlist/{id}', 'categoryController@update');
Route::delete('delete/catlist/{id}', 'categoryController@destroy');


Route::get('/Doc','DocController@create');
Route::post('Doc','DocController@store');
Route::get('/doclist','DocController@index');
Route::get('documents/{id}','DocController@open');
Route::get('documents/download/{id}','docController@download');


Route::post('/search','docController@search');