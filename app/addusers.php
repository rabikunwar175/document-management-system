<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Laratrust\Traits\LaratrustUserTrait;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;



class addusers extends Authenticatable
{
    use Notifiable;
    use LaratrustUserTrait; // add this trait to your user model


    //
    public function department() {
        return $this->belongsTo('App\Department');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}