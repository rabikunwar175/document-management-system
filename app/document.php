<?php

namespace App;
use App\category;
use App\User;

use Illuminate\Database\Eloquent\Model;

class document extends Model
{
    //

     protected $fillable = ['name','description'];

     
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function categories() {
        return $this->belongsToMany('App\Category');
    }
    
     public function department() {
        return $this->belongsTo('App\Department');
    }
}