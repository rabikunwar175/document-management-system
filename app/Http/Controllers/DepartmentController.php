<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;

class DepartmentController extends Controller
{
    public function create()
    {
        return view('Departments.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
    'dptName' => 'required',
      ]);


        $dept=new \App\Department;
        $dept->dptName=$request->get('dptName');
        $dept->save();
        return redirect('/departments')->with('success','Department added Successfully!!!');
    }

    public function index()
    {
        $depts=Department::all();
        return view('Departments.index',compact('depts'));


    }

    public function edit($id)
    {
        $depts=Department::findorFail($id);
        return view('Departments.edit',compact('depts','id'));

    }

    public function update(Request $request,$id)
    {
        $depts = Department::find($id);
        $depts->dptname = $request->input('dptName');
        $depts->save();
        return redirect('/deptlist')->with('success','Updated Successfully!!!');
 }
 public function destroy($id)
 {
     $depts=Department::find($id);
     $depts->delete();
     return redirect('/deptlist')->with('success','Department Has Been Deleted!!!');

 }
}