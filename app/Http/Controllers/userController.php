<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\User;
use App\addusers;

class userController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }
    public function create()

    {
        $departments=Department::all();
        return view('users.create',compact('departments'));

    }  

    public function store(Request $request)
    {
         $this->validate($request, [
          'name' => 'required|string|max:255',
          'email' => 'required|string|email|max:255|unique:users',
          'password' => 'required|string|min:6',
          'department_id' => 'required',
          
        ]);
        $users=new addusers();
        $users->name=$request->get('name');
        $users->email=$request->get('email');
        $users->password=$request->get('password');
        $users->department_id=$request->get('department_id');
        $users->save();
        return redirect('/user')->with('success','User Added Successfully!!!');

    }

    public function index()
    {
        $users=addusers::all();
        $depts=User::all();
        // $depts=Department::all();
        return view('users.index',compact('users','depts'));
    }
    public function edit($id)
 {
     $dpts=Department::all();
     $users=Department::findorFail($id);
     
     
     return view('users.edit',compact('users','dpts','id'));
 }

 public function update(Request $request, $id)
 {
        $users = addusers::find($id);
        $users->name = $request->input('name');
        $users->email = $request->input('email');
        $users->department_id = $request->input('department_id');
        $users->save();
        return redirect('/list')->with('success','Updated Successfully!!!');
 }
 public function destroy($id)
 {
     $users=addusers::find($id);
     $users->delete();
     return redirect('/list')->with('success','User Has Been Deleted!!!');

 }
}