<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;

class categoryController extends Controller
{
    public function create()
    {
        return view('Category.create');
    }

    public function store(Request $request)
    {
         $this->validate($request, [
            'name' => 'required'
        ]);
        $cat=new category;
        $cat->name=$request->get('name');
        $cat->save();
        return redirect('category')->with('success','category created successfully');




    }

    public function index()
    {
        $cats=category::all();
        return view('Category.index',compact('cats'));


    }

    public function edit($id)
    {
        $cats=category::findorFail($id);
        return view('Category.edit',compact('cats','id'));

    }

    public function update(Request $request,$id)
    {
        $cats = category::find($id);
        $cats->name = $request->input('name');
        $cats->save();
        return redirect('/catlist')->with('success','Updated Successfully!!!');
 }
 public function destroy($id)
 {
     $cats=category::find($id);
     $cats->delete();
     return redirect('/catlist')->with('success','Category Has Been Deleted!!!');

 }
    
}