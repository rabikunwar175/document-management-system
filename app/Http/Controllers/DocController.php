<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use App\document;
use App\Department;
use App\User;
use Illuminate\Support\Facades\Storage;
use DB;


class DocController extends Controller
{
    //
      public function __construct()
    {
        $this->middleware('auth');
    }
     public function create()
    {
        $categories = category::all();
        $departments=Department::all();
        return view('Document.create',compact('categories','departments'));
    }

    public function store(Request $request)
    {
         $this->validate($request, [
          'name' => 'required|string|max:255',
          'description' => 'required|string|max:255',
          'file' => 'required|max:50000',
        ]);
       


        // get the data of uploaded user
        $user_id = auth()->user()->id;
        // $department_id = auth()->user()->department_id;
      

        // handle file upload
        if ($request->hasFile('file')) {
            // filename with extension
            $fileNameWithExt = $request->file('file')->getClientOriginalName();
            // filename
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // extension
            $extension = $request->file('file')->getClientOriginalExtension();
            // filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // upload file
            $path = $request->file('file')->storeAs('public/files/'.$user_id, $fileNameToStore);
        }

        $doc = new document;
        $doc->name = $request->input('name');
        $doc->description = $request->input('description');
        $doc->category_id =$request->input('category_id');
        $doc->user_id = $user_id;
        $doc->department_id =$request->input('department_id');
        $doc->file = $path;
        $doc->mimetype = Storage::mimeType($path);
        $size = Storage::size($path);
        if ($size >= 1000000) {
          $doc->filesize = round($size/1000000) . 'MB';
        }elseif ($size >= 1000) {
          $doc->filesize = round($size/1000) . 'KB';
        }else {
          $doc->filesize = $size;
        }
        // determine whether it expires
        if ($request->input('isExpire') == true) {
            $doc->isExpire = false;
        }else {
            $doc->isExpire = true;
            $doc->expires_at = $request->input('expires_at');
        }
        // save to db
        $doc->save();
        // add Category
        // $doc->categories()->sync($request->category_id);

        return redirect('/Doc')->with('success','File Uploaded');
    }

    public function index()
    {
          if (auth()->user()->hasRole('superadministrator'))
        {
            // get all
            $docs = document::where('isExpire','!=',2)->get();
        }
        else
        {
           
           $docs = document::where('isExpire','!=',2)->where('user_id','!=',auth()->user()->id)->get();
        }
        $depts = User::all();

        $filetype = null;

        return view('Document.index',compact('docs','filetype'));
    }

    public function open($id)
    {
        $doc = document::findOrFail($id);
        $path = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix($doc->file);
        $type = $doc->mimetype;

        if ($type == 'application/pdf' || $type == 'image/jpeg' ||
        $type == 'image/png' || $type == 'image/jpg' || $type == 'image/gif')
        {
            return response()->file($path, ['Content-Type' => $type]);
        }
       
        else {
            return response()->file($path, ['Content-Type' => $type]);
        }
    }

      public function download($id)
    {
        $doc = document::findOrFail($id);
        $path = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix($doc->file);
        $type = $doc->mimetype;


        // return response()->download($path, $doc->name, ['Content-Type:' . $type]);
        return response()->download($path);
    }

    public function search(Request $request)
    {
        $this->validate($request,[
          'search' => 'required|string'
        ]);

        $srch = strtolower($request->input('search'));
        $names = document::pluck('name')->all();
        $results = [];

        for ($i=0; $i < count($names); $i++) {
          $lower = strtolower($names[$i]);
          if (strpos($lower, $srch) !== false) {
            $results[$i] = document::where('name', $names[$i])->get();
          }
        }

        return view('Document.result', compact('results'));
    }

    

    
}