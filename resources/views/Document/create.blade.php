@extends('layouts.app1')
@section('content')
<div class="col-md-8">
    <div class="card-group">
        <div class="card">
            <div class="card-body p-5">
                <div class="text-center d-lg-none">
                    <img src="svg/modulr.svg" class="mb-5" width="150" alt="Modulr Logo">
                </div>
                <h1>{{ __('') }}</h1>
                <p class="text-muted">Add Documents</p>

                <form method="POST" action=" " enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="icon-user"></i>
                            </span>
                        </div>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                            name="name" value="{{ old('name') }}" placeholder="{{ __('Name') }}" required autofocus>

                        @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"></span>
                        </div>
                        <input id="description" type="textarea" class="form-control" name="description"
                            placeholder="Description" required autofocus>

                    </div>
                    <select name="department_id" class="form-control" id="department_id" placeholder="select department"
                        required>
                        <option value="">-- Select Department --</option>
                        @foreach ($departments as $department)
                        <option value="{{ $department->id }}">{{ucfirst($department->dptName) }}</option>
                        @endforeach
                    </select>
                    <br>

                    <select name="category_id" class="form-control" id="category_id" placeholder="select Category">
                        <option value="">-- Select Category --</option>
                        @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ucfirst($category->name) }}</option>
                        @endforeach
                    </select>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"></span>
                        </div>
                        <label for="category">Choose (< 50MB) </label> <input id="file" type="file"
                                class="file-path validate" name="file" placeholder="File" required autofocus>

                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"></span>
                        </div>
                        <label for="date">Expires At</label>
                        <input id="date" type="date" class="form-control" name="expires_at">
                    </div>

                    <div class="input-group mb-3">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="isExpire" id="isExpire"
                                {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp{{ __('Doesnot Expire') }}
                            </label>
                        </div>
                    </div>


                    <br>


                    <div class="row">
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary px4">
                                {{ __('ADD') }}
                            </button>
                        </div>

                    </div>
                </form>
            </div>


            @endsection