@extends('layouts.app1')

@section('content')
<style>
.card-content2 {
    padding: 10px 7px;
}

.search {
    width: 30%;
    margin-left: 50px;
    float: right;
}

@media (max-width: 768px) {
    .search {
        width: 50%;
    }
}

@media (max-width: 460px) {
    .search {
        width: 100%;
    }
}

/* --- for right click menu --- */
*,
*::before,
*::after {
    box-sizing: border-box;
}

.task i {
    color: orange;
    font-size: 35px;
}

/* context-menu */
.context-menu {
    padding: 0 5px;
    margin: 0;
    background: #f7f7f7;
    font-size: 15px;
    display: none;
    position: absolute;
    z-index: 10;
    box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
}

.context-menu--active {
    display: block;
}

.context-menu_items {
    margin: 0;
}

.context-menu_item {
    border-bottom: 1px solid #ddd;
    padding: 12px 30px;
}

.context-menu_item:last-child {
    border-bottom: none;
}

.context-menu_item:hover {
    background: #fff;
}

.context-menu_item i {
    margin: 0;
    padding: 0;
}

.context-menu_item p {
    display: inline;
    margin-left: 10px;
}
</style>

<div class="card ">
    <h6 class="flow-text orange-text">{{ count($results) }} Results</h6>
    <div class="card-content">
        <div class="row">
            <div class="left">

            </div>
            <div class="search input-field">
                <form action="/search" method="post" id="search-form">
                    {{ csrf_field() }}

                    <input type="text" name="search" id="search" placeholder="Search Here ...">
                    <label for="search"></label>
                </form>
            </div>
        </div>
        <br>
        <div class="row">
            @if(count($results) > 0)
            @foreach($results as $res)
            @foreach($res as $r)
            <div class="col m2 s6">
                <a href="documents/{{ $r->id }}">
                    <div class="card hoverable indigo lighten-5
		 task" data-id="{{ $r->id }}">

                        <div class="card-content2 center">
                            @if($r->mimetype == "image/jpeg")
                            <i class="material-icons">image</i>
                            @elseif($r->mimetype == "video/mp4")
                            <i class="material-icons">movie</i>
                            @elseif($r->mimetype == "audio/mpeg")
                            <i class="material-icons">music_video</i>
                            @else
                            <i class="material-icons">folder</i>
                            @endif
                            <h6>{{ $r->name }}</h6>
                            <p>{{ $r->filesize }}</p>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
            @endforeach
            <br>
            @else
            &nbsp; &nbsp; &nbsp; <h5 class="text">No Matches Found :(</h5>
            @endif
        </div>
    </div>
</div>


@endsection