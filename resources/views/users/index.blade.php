@extends('layouts.app')
@section('content')
<h2>Users</h2>


<table class="table table-striped">
    <thead>
        <tr>
            <!-- <th>ID</th> -->
            <th>Name</th>
            <th>Email</th>
            <th>Department</th>
            <th>Role</th>
            <th colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>

        @foreach($depts as $dept)

        <tr>

            <!-- <td>{{$dept['id']}}</td> -->
            <td>{{$dept['name']}}</td>
            <td>{{$dept['email']}}</td>
            <td>Administrator</td>
            <td>Admin</td>


        </tr>
        @endforeach

        @foreach($users as $user)
        <tr>
            <!-- <td>{{$user['id']}}</td> -->
            <td>{{$user['name']}}</td>
            <td>{{$user['email']}}</td>
            <td>{{$user->department['dptName']}}</td>
            <td>User</td>
            <td><a href="{{action('userController@edit', $user['id'])}}"><i class="fas fa-pencil-alt"></i></a></td>
            <td>
                <form action="{{action('userController@destroy', $user['id'])}}" method="post">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>

        </tr>
        @endforeach


    </tbody>
</table>
</form>
@endsection