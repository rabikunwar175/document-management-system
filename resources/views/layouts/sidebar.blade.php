<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link active" href="/dashboard">
                    <i class="nav-icon icon-speedometer"></i> Dashboard
                </a>
            </li>
            <li class="nav-title">Settings</li>
            <li class="nav-item">
                <a class="nav-link" href="/user">
                    <i class="nav-icon icon-user"></i> Add Users
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/list">
                    <i class="nav-icon icon-user"></i> Manage Users
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/departments">
                    <i class="nav-icon fa fa-building"></i> Add Department
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/deptlist">
                    <i class="nav-icon fa fa-building"></i> Manage Department
                </a>
            </li>
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>