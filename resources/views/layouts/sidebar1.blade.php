<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link active" href="/dashboard">
                    <i class="nav-icon icon-speedometer"></i> Dashboard
                </a>
            </li>
            <li class="nav-title">Settings</li>
            <li class="nav-item">
                <a class="nav-link" href="/Doc">
                    <i class="nav-icon icon-user"></i> Add Documents
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/doclist">
                    <i class="nav-icon icon-user"></i> Manage Documents
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/category">
                    <i class="nav-icon fa fa-building"></i> Add Category
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/catlist">
                    <i class="nav-icon fa fa-building"></i> Manage Category
                </a>
            </li>
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>