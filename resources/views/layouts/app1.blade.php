<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{asset('js/app.js')}}"></script>
</head>

<body>


    <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
        <div id="app">
            @include('layouts.error')
            @include('layouts.header1')

            <div class="app-body">
                @include('layouts.sidebar1')
                <main class="main">
                    <div class="container-fluid">
                        <div class="animated fadeIn">
                            @yield('content')
                        </div>
                    </div>
                </main>
            </div>
        </div>

    </body>
</body>

</html>