@extends('layouts.app1')
@section('content')
<h2>Users</h2>


<table class="table table-striped">
    <thead>
        <tr>

            <th>Name</th>
            <th colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>

        @foreach($cats as $cat)
        <tr>
            <td>{{$cat['name']}}</td>
            <td><a href="{{action('categoryController@edit', $cat['id'])}}"><i class="fas fa-pencil-alt"></i></a></td>
            <td>
                <form action="{{action('categoryController@destroy', $cat['id'])}}" method="post">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection