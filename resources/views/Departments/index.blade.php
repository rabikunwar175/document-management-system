@extends('layouts.app')
@section('content')
<h2>Department List</h2>


<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th colspan="2">Action</th>
        </tr>
    </thead>
    <tbody>

        @foreach($depts as $dept)
        <tr>
            <td>{{$dept['dptName']}}</td>
            <td><a href="{{action('DepartmentController@edit', $dept['id'])}}"><i class="fas fa-pencil-alt"></i></a>
            </td>
            <td>
                <form action="{{action('DepartmentController@destroy', $dept['id'])}}" method="post">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection