@extends('layouts.app')
@section('content')
<div class="col-md-8">
    <div class="card-group">
        <div class="card">
            <div class="card-body p-5">
                <div class="text-center d-lg-none">
                    <img src="svg/modulr.svg" class="mb-5" width="150" alt="Modulr Logo">
                </div>
                <h1>{{ __('Department') }}</h1>
                <p class="text-muted">Add Department</p>

                <form method="POST" action="{{action('DepartmentController@update',$id)}}">
                    {{csrf_field()}}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"></span>
                        </div>
                        <input id="department" type="text" class="form-control" name="dptName"
                            value="{{$depts->dptName}}" placeholder="{{ __('Department') }}" required autofocus>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <button type="submit" name="submit" class="btn btn-primary px4">
                                {{ __('ADD') }}
                            </button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection